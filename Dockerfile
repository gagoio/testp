FROM node:8

WORKDIR /tmp

ADD . /tmp
RUN npm install
RUN ["sh", "-c", "node ./specs/start.js ./specs/async.spec.js"]

CMD ["sh", "-c", "node ./specs/start.js ./specs/async.spec.js"]